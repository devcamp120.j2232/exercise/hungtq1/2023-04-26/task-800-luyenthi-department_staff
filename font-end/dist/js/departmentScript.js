"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// mảng chapter
var gListDepartmentObjects = [];
var gDepartmentObject = {};
var gSTT = 0;
var gId;

const gBASE_URL = "http://localhost:8080/department";

const ARRAY_NAME_COL = [
  "Stt",
  "departmentCode",
  "departmentName",
  "major",
  "introduction",
  "action",
];
//khai báo các cột
const gCOL_NO = 0;
const gCOL_DEPARTMENT_CODE = 1;
const gCOL_DEPARTMENT_NAME = 2;
const gCOL_MAJOR = 3;
const gCOL_INTRODUCTION = 4;
const gCOL_ACTION = 5;

// khởi tạo datatable  - chưa có data
var gDataTable = $("#department-table").DataTable({
  columns: [
    { data: ARRAY_NAME_COL[gCOL_NO] },
    { data: ARRAY_NAME_COL[gCOL_DEPARTMENT_CODE] },
    { data: ARRAY_NAME_COL[gCOL_DEPARTMENT_NAME] },
    { data: ARRAY_NAME_COL[gCOL_MAJOR] },
    { data: ARRAY_NAME_COL[gCOL_INTRODUCTION] },
    { data: ARRAY_NAME_COL[gCOL_ACTION] },
  ],
  columnDefs: [
    //định nghĩa các cột cần hiện ra
    {
      targets: gCOL_NO, //cột STT
      className: "text-center text-primary",
      /*render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1; */
      render: function () {
        gSTT++;
        return gSTT;
      },
    },
    {
      targets: gCOL_DEPARTMENT_CODE,
      className: "text-center",
    },
    {
      targets: gCOL_ACTION, //cột Action
      className: "text-center",
      defaultContent: `
                            <button id= "update-department"  <i title = 'DEATIL' class="fa-solid fa-file-circle-check text-info" style = "cursor:pointer; border: 1px white;"></i></button> &nbsp;
                            <button id= "delete-department" <i title = 'Delete' class="fa-solid fa-trash text-danger" style = "cursor:pointer;border: 1px white;"></i></button>`,
    },
  ],
});

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();

//gán sự kiện tạo department mới
$("#add-infor-department").on("click", function () {
  onBtnAddInforClick();
});
// gán sự kiện cho nút Confirm create (trên modal)
$("#btn-create-confirm").on("click", function () {
  onBtnConfirmCreateClick();
});
// gán sự kiện cho nút Cancel (trên modal)
$("#btn-create-close").on("click", function () {
  onBtnCloseCreateClick();
});

// 3 - U: gán sự kiện Update - khi nhấn nút edit
$("#department-table").on("click", "#update-department", function () {
  onBtnEditClick(this);
});
// gán sự kiện cho nút Confirm edit (trên modal)
$("#btn-update-confirm").on("click", function () {
  onBtnConfirmEditClick();
});

$("#department-table").on("click", "#delete-department", function () {
  onBtnDeleteClick(this);
});
// gán sự kiện cho nút Confirm edit (trên modal)
$("#btn-delete-confirm").on("click", function () {
  onBtnConfirmDeleteClick();
});
// gán sự kiện cho nút Cancel (trên modal)
$("#btn-delete-cancel").on("click", function () {
  onBtnCancelDeleteClick();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  "use strict";
  // lấy data từ server
  callApiToGetListData();
}

//hàm call API to get all chapter
function callApiToGetListData() {
  $.ajax({
    url: gBASE_URL + "/all",
    type: "GET",
    dataType: "json",
    success: function (responseObject) {
      gListDepartmentObjects = responseObject;
      console.log(responseObject);
      DisplayDataToTable(responseObject);
    },
    error: function (error) {
      console.assert(error.responseText);
    },
  });
}

//hàm xử lý sự kiện add infor chapter
function onBtnAddInforClick() {
  "use strict";
  console.log("Button add được click");
  $("#create-department-modal").modal("show");
}

// Hàm xử lý sự kiện khi icon close trên bảng đc click
function onBtnCloseCreateClick() {
  "use strict";
  $("#create-department-modal").modal("hide");
}

//Hàm xử lý khi nhấn nút confirm trên modal
function onBtnConfirmCreateClick() {
  // khai báo đối tượng
  var vObjData = {
    departmentCode: "",
    departmentName: "",
    major: "",
    introduction: "",
  };
  // B1: Thu thập dữ liệu
  getCreateData(vObjData);
  // B2: Validate update
  var vIsValidate = validateData(vObjData);
  if (vIsValidate) {
    // B3: update order
    $.ajax({
      url: gBASE_URL + "/create",
      type: "POST",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(vObjData),
      success: function (paramRes) {
        // B4: xử lý front-end
        handleCreateSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.responseText);
      },
    });
  }
}

// Hàm xử lý sự kiện khi icon edit trên bảng đc click
function onBtnEditClick(paramBtnEdit) {
  "use strict";
  console.log("Button edit được click");
  // hàm dựa vào button edit (edit or delete) xác định đc id
  var vTableRow = $(paramBtnEdit).parents("tr");
  var vRowData = gDataTable.row(vTableRow).data();
  gId = vRowData.id;

  console.log("ID lấy được: " + gId);
  getChapterById(gId);
}

//Hàm xử lý khi nhấn nút confirm edit trên modal
function onBtnConfirmEditClick() {
  // khai báo đối tượng
  var vObjData = {
    departmentCode: "",
    departmentName: "",
    major: "",
    introduction: "",
  };
  // B1: Thu thập dữ liệu
  getUpdateData(vObjData);
  // B2: Validate update
  var vIsValidate = validateData(vObjData);
  if (vIsValidate) {
    // B3: update
    $.ajax({
      url: gBASE_URL + "/update/" + gId,
      type: "PUT",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(vObjData),
      success: function (paramRes) {
        // B4: xử lý front-end
        handleUpdateSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.responseText);
      },
    });
  }
}

// Hàm xử lý sự kiện khi icon delete trên bảng đc click
function onBtnDeleteClick(paramBtnDelete) {
  "use strict";
  console.log("Button delete được click");
  // hàm dựa vào button detail (edit or delete) xác định đc id
  var vTableRow = $(paramBtnDelete).parents("tr");
  var vRowData = gDataTable.row(vTableRow).data();
  gId = vRowData.id;
  console.log("ID lấy được: " + gId);
  // hiển thị modal lên
  $("#delete-department-modal").modal("show");
}

//sự kiện confirm delete order
function onBtnConfirmDeleteClick() {
  deleteChapterById(gId);
}
//cancel delete order
function onBtnCancelDeleteClick() {
  $("#delete-department-modal").modal("hide");
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm load dữ liệu vào user table
function DisplayDataToTable(paramData) {
  "use strict";
  gSTT = 0; // thiết lập lại stt
  $("#department-table").DataTable().clear(); // xóa dữ liệu
  $("#department-table").DataTable().rows.add(paramData); //thêm các dòng dữ liệu vào datatable
  $("#department-table").DataTable().draw(); //vẽ lại bảng
}

// hàm get chapter by id
function getChapterById(paramId) {
  $.ajax({
    url: gBASE_URL + "/detail/" + paramId,
    type: "GET",
    success: function (paramRes) {
      gDepartmentObject = paramRes;
      showDataToModalUpdate(paramRes);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    },
  });
  // hiển thị modal lên
  $("#update-department-modal").modal("show");
}

// hàm show obj lên modal update
function showDataToModalUpdate(paramObj) {
  $("#update-departmentCode").val(paramObj.departmentCode);
  $("#update-departmentName").val(paramObj.departmentName);
  $("#update-major").val(paramObj.major);
  $("#update-introduction").val(paramObj.introduction);
}

// hàm thu thập dữ liệu để update
function getCreateData(paramObj) {
  paramObj.departmentCode = $("#create-departmentCode").val().trim();
  paramObj.departmentName = $("#create-departmentName").val().trim();
  paramObj.major = $("#create-major").val().trim();
  paramObj.introduction = $("#create-introduction").val().trim();
}

// hàm validate data
function validateData(paramObj) {
  if (paramObj.departmentCode === "") {
    alert("Please fill department code!");
    return false;
  }
  for (var i = 0; i < gListDepartmentObjects.length; i++) {
    if (
      paramObj.departmentCode == gListDepartmentObjects[i].departmentCode &&
      gId != gListDepartmentObjects[i].id
    ) {
      alert("department code already exists!");
      return false;
    }
  }
  if (paramObj.departmentName === "") {
    alert("Please fill department name!");
    return false;
  }
  if (paramObj.major === "") {
    alert("Please fill major!");
    return false;
  }
  if (paramObj.introduction === "") {
    alert("Please fill introduction!");
    return false;
  }
  return true;
}

//hàm confirm create thành công
function handleCreateSuccess() {
  alert("Confirmed create data department successfully!");
  callApiToGetListData();
  resetCreateForm();
  $("#create-department-modal").modal("hide");
}

//hàm confirm edit thành công
function handleUpdateSuccess() {
  alert("Confirmed update data department successfully!");
  callApiToGetListData();
  resetEditForm();
  $("#update-department-modal").modal("hide");
}

// hàm xóa trắng form edit modal
function resetCreateForm() {
  $("#create-departmentCode").val("");
  $("#create-departmentName").val("");
  $("#create-major").val("");
  $("#create-introduction").val("");
}

// hàm xóa trắng form edit modal
function resetEditForm() {
  $("#update-departmentCode").val("");
  $("#update-departmentName").val("");
  $("#update-major").val("");
  $("#update-introduction").val("");
  location.reload();
}

// hàm thu thập dữ liệu để update chapter
function getUpdateData(paramObj) {
  paramObj.departmentCode = $("#update-departmentCode").val().trim();
  paramObj.departmentName = $("#update-departmentName").val().trim();
  paramObj.major = $("#update-major").val().trim();
  paramObj.introduction = $("#update-introduction").val().trim();
}

// hàm get chapter by id
function deleteChapterById(paramId) {
  $.ajax({
    url: gBASE_URL + "/delete/" + paramId,
    type: "DELETE",
    async: false,
    success: function (paramRes) {
      console.log(paramRes);
      handleDeleteSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    },
  });
}

//hàm confirm delete thành công
function handleDeleteSuccess() {
  alert("Delete data department successfully!");
  callApiToGetListData();
  $("#delete-department-modal").modal("hide");
}
