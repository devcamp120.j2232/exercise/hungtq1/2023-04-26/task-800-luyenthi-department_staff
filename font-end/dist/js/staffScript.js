"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
// mảng chapter
var gListDepartmentObjects = [];
var gListStaffObjects = [];
var gStaffObject = {};
var gSTT = 0;
var gIdStaff;
var gIdDepartment;

const gBASE_URL = "http://localhost:8080/staff";

const ARRAY_NAME_COL = [
  "Stt",
  "staffCode",
  "staffName",
  "position",
  "sex",
  "birthday",
  "address",
  "phone",
  "action",
];
//khai báo các cột
const gCOL_NO = 0;
const gCOL_STAFF_CODE = 1;
const gCOL_STAFF_NAME = 2;
const gCOL_POSITION = 3;
const gCOL_SEX = 4;
const gCOL_BIRTHDAY = 5;
const gCOL_ADDRESS = 6;
const gCOL_PHONE = 7;
const gCOL_ACTION = 8;

// khởi tạo datatable  - chưa có data
var gDataTable = $("#staff-table").DataTable({
  columns: [
    { data: ARRAY_NAME_COL[gCOL_NO] },
    { data: ARRAY_NAME_COL[gCOL_STAFF_CODE] },
    { data: ARRAY_NAME_COL[gCOL_STAFF_NAME] },
    { data: ARRAY_NAME_COL[gCOL_POSITION] },
    { data: ARRAY_NAME_COL[gCOL_SEX] },
    { data: ARRAY_NAME_COL[gCOL_BIRTHDAY] },
    { data: ARRAY_NAME_COL[gCOL_ADDRESS] },
    { data: ARRAY_NAME_COL[gCOL_PHONE] },
    { data: ARRAY_NAME_COL[gCOL_ACTION] },
  ],
  columnDefs: [
    //định nghĩa các cột cần hiện ra
    {
      targets: gCOL_NO, //cột STT
      className: "text-center text-primary",
      /*render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1; */
      render: function () {
        gSTT++;
        return gSTT;
      },
    },
    {
      targets: gCOL_STAFF_CODE,
      className: "text-center",
    },
    {
      targets: gCOL_ACTION, //cột Action
      className: "text-center",
      defaultContent: `
                            <button id= "update-staff"  <i title = 'DEATIL' class="fa-solid fa-file-circle-check text-info" style = "cursor:pointer; border: 1px white;"></i></button> &nbsp;
                            <button id= "delete-staff" <i title = 'Delete' class="fa-solid fa-trash text-danger" style = "cursor:pointer;border: 1px white;"></i></button>`,
    },
  ],
});

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
onPageLoading();

//gán sự kiện tạo staff mới
$("#add-infor-staff").on("click", function () {
  onBtnAddInforClick();
});
// gán sự kiện cho nút Confirm create (trên modal)
$("#btn-create-confirm").on("click", function () {
  onBtnConfirmCreateClick();
});
// gán sự kiện cho nút Cancel (trên modal)
$("#btn-create-close").on("click", function () {
  onBtnCloseCreateClick();
});

// 3 - U: gán sự kiện Update - khi nhấn nút edit
$("#staff-table").on("click", "#update-staff", function () {
  onBtnEditClick(this);
});
// gán sự kiện cho nút Confirm edit (trên modal)
$("#btn-update-confirm").on("click", function () {
  onBtnConfirmEditClick();
});

$("#staff-table").on("click", "#delete-staff", function () {
  onBtnDeleteClick(this);
});
// gán sự kiện cho nút Confirm edit (trên modal)
$("#btn-delete-confirm").on("click", function () {
  onBtnConfirmDeleteClick();
});
// gán sự kiện cho nút Cancel (trên modal)
$("#btn-delete-cancel").on("click", function () {
  onBtnCancelDeleteClick();
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  "use strict";
  // lấy data từ server
  callApiToGetListData();
  callApiToGetListDepartment();
}

//Get data chapter
function callApiToGetListDepartment() {
  $.ajax({
    url: "http://localhost:8080/department/all",
    method: "GET",
    success: function (pObjRes) {
      gListDepartmentObjects = pObjRes;
      console.log(pObjRes);
      loadDataTodepartmentSelect(pObjRes);
    },
    error: function (pXhrObj) {
      console.log(pXhrObj.responseText);
    },
  });
}

var gDepartmentCreateSelect = $("#create-departmentSelect");
var gDepartmentCreateSelect = $("#update-departmentSelected");

function loadDataTodepartmentSelect(pChapterList) {
  for (var i = 0; i < pChapterList.length; i++) {
    var bDepartmentOption = $("<option/>");
    bDepartmentOption.prop("value", pChapterList[i].id);
    bDepartmentOption.prop("text", pChapterList[i].departmentName);
    gDepartmentCreateSelect.append(bDepartmentOption);
  }
  for (var i = 0; i < pChapterList.length; i++) {
    var bDepartmentOption = $("<option/>");
    bDepartmentOption.prop("value", pChapterList[i].id);
    bDepartmentOption.prop("text", pChapterList[i].departmentName);
    gDepartmentCreateSelect.append(bDepartmentOption);
  }
}

//hàm call API to get all staff
function callApiToGetListData() {
  $.ajax({
    url: gBASE_URL + "/all",
    type: "GET",
    dataType: "json",
    success: function (responseObject) {
      gListStaffObjects = responseObject;
      console.log(responseObject);
      DisplayDataToTable(responseObject);
    },
    error: function (error) {
      console.assert(error.responseText);
    },
  });
}

//hàm xử lý sự kiện add infor article
function onBtnAddInforClick() {
  "use strict";
  console.log("Button add được click");
  $("#create-staff-modal").modal("show");
}

// Hàm xử lý sự kiện khi icon close trên bảng đc click
function onBtnCloseCreateClick() {
  "use strict";
  $("#create-staff-modal").modal("hide");
}

//Hàm xử lý khi nhấn nút confirm trên modal
function onBtnConfirmCreateClick() {
  // khai báo đối tượng
  var vObjData = {
    staffCode: "",
    staffName: "",
    position: "",
    sex: "",
    birthday: "",
    address: "",
    phone: "",
    departmentId: 0,
  };
  // B1: Thu thập dữ liệu
  getCreateData(vObjData);
  // B2: Validate update
  var vIsValidate = validateData(vObjData);
  if (vIsValidate) {
    // B3: update order
    $.ajax({
      url: gBASE_URL + "/create/" + vObjData.departmentId,
      type: "POST",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(vObjData),
      success: function (paramRes) {
        // B4: xử lý front-end
        handleCreateSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.responseText);
      },
    });
  }
}

// Hàm xử lý sự kiện khi icon edit trên bảng đc click
function onBtnEditClick(paramBtnEdit) {
  "use strict";
  console.log("Button edit được click");
  // hàm dựa vào button edit (edit or delete) xác định đc id
  var vTableRow = $(paramBtnEdit).parents("tr");
  var vRowData = gDataTable.row(vTableRow).data();
  gIdStaff = vRowData.id;
  getIdDepartmentByIdStaff();
  console.log("ID staff lấy được: " + gIdStaff);
  getArticleById(gIdStaff);
}

//Hàm xử lý khi nhấn nút confirm edit trên modal
function onBtnConfirmEditClick() {
  // khai báo đối tượng
  var vObjData = {
    staffCode: "",
    staffName: "",
    position: "",
    sex: "",
    birthday: "",
    address: "",
    phone: "",
    departmentId: 0,
  };
  // B1: Thu thập dữ liệu
  getUpdateData(vObjData);
  // B2: Validate update
  var vIsValidate = validateData(vObjData);
  if (vIsValidate) {
    // B3: update
    $.ajax({
      url: gBASE_URL + "/update/" + vObjData.departmentId + "/" + gIdStaff,
      type: "PUT",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(vObjData),
      success: function (paramRes) {
        // B4: xử lý front-end
        handleUpdateSuccess();
      },
      error: function (paramErr) {
        console.log(paramErr.responseText);
      },
    });
  }
}

// Hàm xử lý sự kiện khi icon delete trên bảng đc click
function onBtnDeleteClick(paramBtnDelete) {
  "use strict";
  console.log("Button delete được click");
  // hàm dựa vào button detail (edit or delete) xác định đc id
  var vTableRow = $(paramBtnDelete).parents("tr");
  var vRowData = gDataTable.row(vTableRow).data();
  gIdStaff = vRowData.id;
  console.log("ID staff lấy được: " + gIdStaff);
  // hiển thị modal lên
  $("#delete-staff-modal").modal("show");
}

//sự kiện confirm delete staff
function onBtnConfirmDeleteClick() {
  deleteArticleById(gIdStaff);
}
//cancel delete staff
function onBtnCancelDeleteClick() {
  $("#delete-staff-modal").modal("hide");
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//hàm load dữ liệu vào data table
function DisplayDataToTable(paramData) {
  "use strict";
  gSTT = 0; // thiết lập lại stt
  $("#staff-table").DataTable().clear(); // xóa dữ liệu
  $("#staff-table").DataTable().rows.add(paramData); //thêm các dòng dữ liệu vào datatable
  $("#staff-table").DataTable().draw(); //vẽ lại bảng
}

// hàm get article by id
function getArticleById(paramId) {
  $.ajax({
    url: gBASE_URL + "/detail/" + paramId,
    type: "GET",
    success: function (paramRes) {
      gStaffObject = paramRes;
      showDataToModalUpdate(paramRes);
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    },
  });
  // hiển thị modal lên
  $("#update-staff-modal").modal("show");
}

//lấy id department từ id staff
function getIdDepartmentByIdStaff() {
  for (var i = 0; i < gListDepartmentObjects.length; i++) {
    var vStaff = gListDepartmentObjects[i].articles;
    for (var j = 0; j < vStaff.length; j++) {
      if (gIdStaff == vStaff[j].id) {
        gIdDepartment = gListDepartmentObjects[i].id;
        console.log("ID department vừa lấy được: " + gIdDepartment);
      }
    }
  }
}

// hàm show obj lên modal update
function showDataToModalUpdate(paramObj) {
  $("#update-staffCode").val(paramObj.staffCode);
  $("#update-staffName").val(paramObj.staffName);
  $("#update-position").val(paramObj.position);
  $("#update-sex").val(paramObj.sex);
  $("#update-birthday").val(paramObj.birthday);
  $("#update-address").val(paramObj.address);
  $("#update-phone").val(paramObj.phone);
  $("#update-departmentSelected").val(gIdDepartment);
}

// hàm thu thập dữ liệu để create
function getCreateData(paramObj) {
  paramObj.staffCode = $("#create-staffCode").val().trim();
  paramObj.staffName = $("#create-staffName").val().trim();
  paramObj.position = $("#create-position").val().trim();
  paramObj.sex = $("#create-sex").val().trim();
  paramObj.birthday = $("#create-birthday").val().trim();
  paramObj.address = $("#create-address").val().trim();
  paramObj.phone = $("#create-phone").val().trim();
  paramObj.departmentId = $("#create-departmentSelect").val();
}

// hàm thu thập dữ liệu để update article
function getUpdateData(paramObj) {
  paramObj.staffCode = $("#update-staffCode").val().trim();
  paramObj.staffName = $("#update-staffName").val().trim();
  paramObj.position = $("#update-position").val().trim();
  paramObj.sex = $("#update-sex").val().trim();
  paramObj.birthday = $("#update-birthday").val().trim();
  paramObj.address = $("#update-address").val().trim();
  paramObj.phone = $("#update-phone").val().trim();
  paramObj.departmentId = $("#update-departmentSelected").val();
}

// hàm validate data
function validateData(paramObj) {
  if (paramObj.staffCode === "") {
    alert("Please fill article code!");
    return false;
  }
  for (var i = 0; i < gListStaffObjects.length; i++) {
    if (
      paramObj.staffCode == gListStaffObjects[i].staffCode &&
      gIdStaff != gListStaffObjects[i].id
    ) {
      alert("Article code already exists!");
      return false;
    }
  }
  if (paramObj.staffName === "") {
    alert("Please fill article name!");
    return false;
  }
  if (paramObj.position === "") {
    alert("Please fill article introduction!");
    return false;
  }
  if (paramObj.sex <= 0) {
    alert("Please fill article page!");
    return false;
  }
  if (paramObj.departmentId == 0) {
    alert("Please select chapter!");
    return false;
  }
  return true;
}

//hàm confirm create thành công
function handleCreateSuccess() {
  alert("Confirmed create data article successfully!");
  callApiToGetListData();
  resetCreateForm();
  $("#create-staff-modal").modal("hide");
}

//hàm confirm edit thành công
function handleUpdateSuccess() {
  alert("Confirmed update data article successfully!");
  callApiToGetListData();
  resetEditForm();
  $("#update-staff-modal").modal("hide");
}

// hàm xóa trắng form edit modal
function resetCreateForm() {
  $("#create-staffCode").val("");
  $("#create-staffName").val("");
  $("#create-position").val("");
  $("#create-sex").val("");
  $("#create-departmentSelect").val(0);
}

// hàm xóa trắng form edit modal
function resetEditForm() {
  $("#update-staffCode").val("");
  $("#update-staffName").val("");
  $("#update-position").val("");
  $("#update-sex").val("");
  $("#update-departmentSelected").val(0);
  location.reload();
}

// hàm get article by id
function deleteArticleById(paramId) {
  $.ajax({
    url: gBASE_URL + "/delete/" + paramId,
    type: "DELETE",
    async: false,
    success: function (paramRes) {
      console.log(paramRes);
      handleDeleteSuccess();
    },
    error: function (paramErr) {
      console.log(paramErr.status);
    },
  });
}

//hàm confirm delete thành công
function handleDeleteSuccess() {
  alert("Delete data article successfully!");
  callApiToGetListData();
  $("#delete-staff-modal").modal("hide");
}
