package com.devcamp.departmentstaff.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.departmentstaff.model.Department;
import com.devcamp.departmentstaff.model.Staff;
import com.devcamp.departmentstaff.repository.IDepartmentRepository;

@Service
public class DepartmentService {
  @Autowired
  IDepartmentRepository pIDepartmentRepository;

  public ArrayList<Department> getAllDepartments() {
    ArrayList<Department> departments = new ArrayList<>();
    pIDepartmentRepository.findAll().forEach(departments::add);
    return departments;
  }

  public Department createDepartment(Department cDepartment) {
    try {
      Department savedRole = pIDepartmentRepository.save(cDepartment);
      return savedRole;
    } catch (Exception e) {
      return null;
    }
  }

  public Set<Staff> getStaffByDepartmentId(long id) {
    Department vDepartment = pIDepartmentRepository.findById(id);
    if (vDepartment != null) {
      return vDepartment.getStaffs();
    } else
      return null;
  }
}
