package com.devcamp.departmentstaff.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.departmentstaff.model.Department;

public interface IDepartmentRepository extends JpaRepository<Department, Long> {
  Department findById(long id);
}
