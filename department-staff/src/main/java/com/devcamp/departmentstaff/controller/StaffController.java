package com.devcamp.departmentstaff.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.departmentstaff.model.Department;
import com.devcamp.departmentstaff.model.Staff;
import com.devcamp.departmentstaff.repository.IDepartmentRepository;
import com.devcamp.departmentstaff.repository.IStaffRepository;
import com.devcamp.departmentstaff.service.DepartmentService;
import com.devcamp.departmentstaff.service.StaffService;

@RestController
@RequestMapping("/staff")
@CrossOrigin
public class StaffController {
  @Autowired
  IDepartmentRepository pIDepartmentRepository;
  @Autowired
  IStaffRepository pIStaffRepository;
  @Autowired
  DepartmentService departmentService;
  @Autowired
  StaffService staffService;

  // get all article list
  @GetMapping("/all")
  public ResponseEntity<List<Staff>> getAllStaffs() {
    try {
      return new ResponseEntity<>(staffService.getAllStaffs(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  // get staff by department id
  @GetMapping("/search/departmentId={id}")
  public ResponseEntity<Set<Staff>> getStaffByDepartmentId(@PathVariable(name = "id") long id) {
    try {
      Set<Staff> departmentStaff = departmentService.getStaffByDepartmentId(id);
      if (departmentStaff != null) {
        return new ResponseEntity<>(departmentStaff, HttpStatus.OK);
      } else
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // get staff detail by id
  @GetMapping("/detail/{id}")
  public ResponseEntity<Object> getArticleById(@PathVariable(name = "id", required = true) Long id) {
    Optional<Staff> staff = pIStaffRepository.findById(id);
    if (staff.isPresent()) {
      return new ResponseEntity<>(staff, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  // create new staff by department
  @PostMapping("/create/{id}")
  public ResponseEntity<Object> createStaff(@PathVariable("id") int id, @Valid @RequestBody Staff newStaff) {
    try {
      Department department = pIDepartmentRepository.findById(id);
      if (department != null) {
        Staff staff = new Staff();
        staff.setStaffCode(newStaff.getStaffCode());
        staff.setStaffName(newStaff.getStaffName());
        staff.setSex(newStaff.getSex());
        staff.setPosition(newStaff.getPosition());
        staff.setPhone(newStaff.getPhone());
        staff.setAddress(newStaff.getAddress());
        staff.setBirthday(newStaff.getBirthday());
        staff.setDepartment(department);
        Staff _staff = pIStaffRepository.save(staff);
        return new ResponseEntity<>(_staff, HttpStatus.CREATED);
      } else
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // update staff
  @PutMapping("/update/{departmentId}/{staffId}")
  public ResponseEntity<Object> updateStaff(@PathVariable(value = "departmentId") long departmentId,
      @PathVariable(value = "staffId") long staffId,
      @Valid @RequestBody Staff updateStaff) {
    try {
      Department department = pIDepartmentRepository.findById(departmentId);
      Staff staffData = pIStaffRepository.findById(staffId);
      Staff staff = staffData;
      staff.setStaffCode(updateStaff.getStaffCode());
      staff.setStaffName(updateStaff.getStaffName());
      staff.setSex(updateStaff.getSex());
      staff.setPosition(updateStaff.getPosition());
      staff.setPhone(updateStaff.getPhone());
      staff.setAddress(updateStaff.getAddress());
      staff.setBirthday(updateStaff.getBirthday());
      staff.setDepartment(department);
      try {
        return new ResponseEntity<>(pIStaffRepository.save(staff), HttpStatus.OK);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Update specified Article: " +
                e.getCause().getCause().getMessage());
      }
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // delete staff by id
  @DeleteMapping("/delete/{id}")
  public ResponseEntity<Staff> deleteStaff(@PathVariable("id") long id) {
    try {
      pIStaffRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
